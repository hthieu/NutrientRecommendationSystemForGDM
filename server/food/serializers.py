from rest_framework import serializers
from .models import Food, DataSource


class DataSourceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = DataSource
        fields = ['name']

class FoodSerializer(serializers.HyperlinkedModelSerializer):
    tags = serializers.SerializerMethodField()
    
    def get_queryset(self, request):
        return super().get_queryset(request).prefetch_related('tags')

    def get_tags(self, obj):
        return u", ".join(o.name for o in obj.tags.all())

    class Meta:
        model = Food
        fields = [
            'id', 'name', 'description', 'image',
            'ingredient', 'date_modified', 'date_created',
            'manufacturer', 'data_source', 'group', 'sub_group', 'tags'
        ]