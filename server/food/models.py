from django.db import models
from django.core.files.storage import FileSystemStorage

from taggit.managers import TaggableManager

def food_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'food_{0}/{1}'.format(instance.id, filename)

class Measure_Unit(models.Model):
    name = models.CharField(max_length=10)
    def __str__(self):
        return self.name

class Ingredient(models.Model):
    name = models.CharField(max_length=100)
    def __str__(self):
        return self.name

class Manufacturer(models.Model):
    name = models.CharField(max_length=50, null=True)
    address = models.CharField(max_length=100, null=True)
    phone = models.CharField(max_length=13, null=True)
    email = models.CharField(max_length=20, null=True)
    website = models.CharField(max_length=25, null=True)
    def __str__(self):
        return self.name    

class DataSource(models.Model):
    name = models.CharField(max_length=50)
    def __str__(self):
        return self.name    


# from sorl.thumbnail import ImageField, get_thumbnail



class Food(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(blank=True, max_length=255)    
    tags = TaggableManager()
    image = models.ImageField(upload_to=food_directory_path)
    # image = ImageField(upload_to=food_directory_path)
    ingredient = models.ManyToManyField(Ingredient, through='IngredientInFood', blank=True, default='')
    date_modified = models.DateTimeField(auto_now_add=True, editable=False)
    date_created = models.DateTimeField(auto_now=True, editable=False)
    manufacturer = models.ForeignKey(Manufacturer, on_delete=models.PROTECT, blank=True, null=True, default='')
    data_source  = models.ForeignKey(DataSource, on_delete=models.PROTECT, blank=True, null=True, default='')
    
    group = models.CharField(max_length=50, blank=True, null=True)        
    sub_group = models.CharField(max_length=50, blank=True, null=True)        
    

    # def save(self, *args, **kwargs):
    #     if self.image:
            # self.image = get_thumbnail(self.image, '500x600', quality=99, format='JPEG').
    #     super(Food, self).save(*args, **kwargs)


    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Food'


class Portion(models.Model):
    name = models.CharField(max_length=30, verbose_name='portion name')
    food_id = models.ForeignKey(Food, on_delete=models.PROTECT, verbose_name='food name')
    amount = models.FloatField(blank=True, default=True, null=True)
    unit = models.ForeignKey(Measure_Unit, on_delete=models.PROTECT, blank=True, null=True)
    
    
    def get_group(self):
        return self.food_id.group

    def get_sub_group(self):
        return self.food_id.sub_group        

    get_group.short_description = 'Group'

    get_sub_group.short_description = 'Subgroup'

    def __str__(self):
        return self.name

    
class NutrientList(models.Model):
    nutrient_name  = models.CharField(unique=True, max_length=100, blank=True)
    category =  models.CharField(max_length=30)
    ordered_no = models.IntegerField(
        verbose_name="Order Number", 
        help_text="Order Number allows we manually change the order of nutrient",
        default=0
    )
    
    def __str__(self):
        return (str(self.nutrient_name))

class Nutrient(models.Model):    
    name = models.ForeignKey(NutrientList, on_delete=models.PROTECT, to_field='nutrient_name')
    amount     = models.FloatField(default=100)
    unit       = models.ForeignKey(Measure_Unit, on_delete=models.PROTECT)
    portion = models.ForeignKey(Portion, on_delete=models.PROTECT, blank=True, null=True)
    
    def __str__(self):
        return str(self.name)

class IngredientInFood(models.Model):
    food = models.ForeignKey(Food, on_delete=models.PROTECT)
    ingredient = models.ForeignKey(Ingredient, on_delete=models.PROTECT)
    created_date = models.DateField(auto_now_add=True, editable=False)
    
    def getFoodName(self):
        self.food.__str__()

    def getIngredientName(self):
        self.food.__str__()        

    created_date.short_description = 'Created Date'