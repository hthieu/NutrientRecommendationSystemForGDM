from .models import Food, DataSource
from rest_framework import viewsets
from .serializers import FoodSerializer, DataSourceSerializer

from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

# Create your views here.

class DataSourceViewSet(viewsets.ModelViewSet):
    queryset = DataSource.objects.all() 
    serializer_class = DataSourceSerializer

    
# class FoodViewSet(viewsets.ViewSet):
#     def list(self, request):
#         queryset = Food.objects.all()
#         serializer = FoodSerializer(queryset,  many=True, context={'request': request})
#         return Response(serializer.data)
    
    
class FoodViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated,]
    queryset = Food.objects.all()
    serializer_class = FoodSerializer
        