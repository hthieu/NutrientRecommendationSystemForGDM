from django.shortcuts import render
from users.models import Patient

# Create your views here.

from datetime import date

PROTEIN_RATIO = 0.8 # the number based on the weights
LIPID_RATIO = 0.2 # OR LIPID_RATIO = 0.35
CARB_RATIO = 0.45

# Solution to calcuate age, 
# source: Danny W. Adair,  https://stackoverflow.com/questions/2217488/age-from-birthdate-in-python
def calculate_age(born):
    today = date.today()
    return today.year - born.year - ((today.month, today.day) < (born.month, born.day))

# Get patient  by the user
def get_patient_by_user_id(user_id):
    patient = Patient.objects.get(pk=user_id)
    return patient

def get_patient_info(user_id):
    patient = get_patient_by_user_id(user_id)    
    age = calculate_age(patient.birth_date)
    height = patient.height / 100
    weight = patient.weight
    activity_level = patient.activity.score # PA
    print("patient_name: " +patient)    
    print("age: " + str(age))
    print("height: " + str(height))
    print("weight: " + str(weight))
    print("activity level: " + str(activity_level))
    '''
    # Estimated Energy Requirement (kcal/day) = Total Energy Expenditure
    #EER = 354 − (6.91 × age [y]) + PA × [(9.36 × weight [kg]) + (726 v height [m])]
    # compared correctly with https://fnic.nal.usda.gov/fnic/dri-calculator/results.php 
    '''
    energy = 354 - (6.91 * age) + activity_level * ( (9.36 * weight) + (726 * height) ) 
    print("EER: " + str(energy))
    ''' calculate nutritional needs '''
    protein =  PROTEIN_RATIO*weight
    ''' carbohydrates should be around 45->65%  '''
    carbohydrate = CARB_RATIO*energy/4

    lipid = LIPID_RATIO*energy/9
        
    print("carb: "+str(carbohydrate))    
    print("protein: " + str(protein))
    print("lipid: " + str(lipid))
    return { "carbohydrate": carbohydrate, "protein": protein, "lipid": lipid }

    #
    # Distribution of food in a day:
    # 1. Breakfast: 20% x Total Energy
    # 2. Food interlude morning: 10-15% x Total Energy
    # 3. Lunch: 30% x Total Energy
    # 4. Interlude afternoon snack: 10-15% x Total Energy
    # 5. Dinner: 25% x Total Energy


    # energy_breakfast = 0.2*energy

    # print(energy_breakfast)

    # from food.models import Food, Portion

    # print (Food.objects.all().filter(name="Rice"))

    # print(get_patient_info(user_id=39))