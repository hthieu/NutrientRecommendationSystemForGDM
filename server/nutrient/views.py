from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from .serializers import SelectedFoodSerializer

@api_view(['POST'])
def calculate_needed_amount_intake(request):
    if request.method == 'POST':
        serializer = SelectedFoodSerializer(data=request.data)
        if serializer.is_valid():
            # serializer.save()
            # call GA calculato here!
            
            #
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)