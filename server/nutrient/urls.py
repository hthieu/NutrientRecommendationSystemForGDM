from .views import calculate_needed_amount_intake
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'calculator', calculate_needed_amount_intake, basename='calculate_needed_amount_intake')
urlpatterns = router.urls