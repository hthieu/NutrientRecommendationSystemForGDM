from rest_framework import serializers

'''
# sample data:
data = {
    "morning": {
        "main": [1,2],
        "dessert": [3,4]        
    },
    "lunch": {
        "main": [2,3],
        "dessert": [2,1]        
    },
    "dinner": {
        "main": [1,4],
        "dessert": [1,3]        
    },   
}
'''
class MealSerializer(serializers.Serializer):
    main = serializers.ListField(child=serializers.IntegerField(min_value=0, max_value=50))
        
    dessert = serializers.ListField(child=serializers.IntegerField(min_value=0, max_value=50))

class SelectedFoodSerializer(serializers.Serializer):
    """Your data serializer, define your fields here."""
    morning = MealSerializer()
    lunch = MealSerializer()
    dinner = MealSerializer()
    