from food.models import Food, Portion, Nutrient

''' usage:
banana = input_ga('7', 'banana')
vegetable = input_ga('9', 'vegetable')
yogurt = input_ga('10', 'yogurt')
rice = input_ga('1', 'white rice')
beef = input_ga('10', 'beef')

selected_food = []
selected_food.append(banana)
selected_food.append(vegetable)
selected_food.append(yogurt)
selected_food.append(rice)
selected_food.append(beef)
print("selected_food:")
print(selected_food)
'''

class input_ga():
    def  __repr__(self):
        return self.food_name
    
    def __init__(self, food_id_query, food_name, low=0, up=3):
        self.food_name = food_name
        self.nutrient = Nutrient.objects.all().filter(
                            portion__food_id=food_id_query).values() # food_id is unique 
        self.energy  = self.nutrient.filter(name_id='Energy')[0]['amount']
        self.protein = self.nutrient.filter(name_id='Protein')[0]['amount']
        self.carb    = self.nutrient.filter(name_id='Carbohydrate, by difference')[0]['amount']
        self.lipid   = self.nutrient.filter(name_id='Total lipid (fat)')[0]['amount']
        self.low = low
        self.up  = up