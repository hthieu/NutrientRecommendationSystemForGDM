from django.shortcuts import render
from users.models import Patient

# Create your views here.

from datetime import date

# Solution to calcuate age, 
# source: Danny W. Adair,  https://stackoverflow.com/questions/2217488/age-from-birthdate-in-python
def calculate_age(born):
    today = date.today()
    return today.year - born.year - ((today.month, today.day) < (born.month, born.day))

# Get patient data
patient = Patient.objects.all()[0]
patient_name = patient.get_full_name()
age = calculate_age(patient.birth_date)
height = patient.height / 100
weight = patient.weight
activity_level = patient.activity.score # PA

# Estimated Energy Requirement (kcal/day) = Total Energy Expenditure
#EER = 354 − (6.91 × age [y]) + PA × [(9.36 × weight [kg]) + (726 v height [m])]
# compared correctly with https://fnic.nal.usda.gov/fnic/dri-calculator/results.php
energy = 354 - (6.91 * age) + activity_level * ( (9.36 * weight) + (726 * height) ) 

print("patient_name")
print(patient_name)
print("age")
print(age)
print("height")
print(height)
print("weight")
print(weight)
print("activity level:")
print(activity_level)
print("EER:")
print(energy)
print()
# calculate nutritional needs

protein =  0.8*weight
# 45->65% 
carb = 0.45*energy/4
carbohydrate = 0.65*energy/4

fat1= fat = 0.2*energy/9
fat2 = 0.35*energy/9
 
print("carb: "+str(carb))
print("carb2: " + str(carbohydrate))
print("protein: " + str(protein))
print("fat1: " + str(fat1))
print("fat2: " + str(fat2))

#
# Distribution of food in a day:
# 1. Breakfast: 20% x Total Energy
# 2. Food interlude morning: 10-15% x Total Energy
# 3. Lunch: 30% x Total Energy
# 4. Interlude afternoon snack: 10-15% x Total Energy
# 5. Dinner: 25% x Total Energy


energy_breakfast = 0.2*energy

print(energy_breakfast)

from food.models import Food, Portion



print (Food.objects.all().filter(name="Rice"))

