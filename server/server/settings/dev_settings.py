import os
import environ # third party library to read environment variables from .env file : https://django-environ.readthedocs.io/en/latest/

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Read environment variables from .env file. Guide are taken from  https://github.com/joke2k/django-environ/tree/master
env = environ.Env(DEBUG=(bool, True))
PATH = os.path.abspath(os.path.dirname(__file__)) + '/.env' # '/home/phatvo/THESIS/NutrientRecommendationSystemForGDM/server/server/settings/.env'
environ.Env.read_env()

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = env('DEBUG')

SECRET_KEY = env('SECRET_KEY')

# ALLOWED_HOSTS = ['10.0.2.2', '127.0.0.1']
ALLOWED_HOSTS = ['*']

CORS_ORIGIN_WHITELIST = (
'http://127.0.0.1:8000',
'http://localhost:8000'
)

CORS_ALLOW_CREDENTIALS = True
CORS_EXPOSE_HEADERS = (
    'Access-Control-Allow-Origin: *',
)


CSRF_COOKIE_HTTPONLY = False
SESSION_COOKIE_SECURE = False
CSRF_COOKIE_SECURE = False
# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites', 
    'corsheaders',
    # Third Party
    'rest_framework',
    
    # 'rest_framework.authtoken',
    
    # these modules allow third party authentication (Facebook, Google ID,..)
    # so far I use the modules to integrate email verification 
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    
    'rest_auth',        
    # 'rest_auth.registration',
    "taggit",
    'sorl.thumbnail',
    # Local 
    'users.apps.UsersConfig',
    'food.apps.FoodConfig',
    'factors.apps.FactorsConfig',
    'django_extensions',
    'patient_menu_calculator',

    'django_inlinecss', # Neccessary for building HTML Email (automatically take content of css file and build into inline css),
    'drf_yasg',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'server.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'server.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'nutrient_db',
        'USER': env('DB_USER'),
        'PASSWORD': env('DB_PASSWORD'),
        'HOST': 'localhost',
        'PORT': '',
    }
}
# import dj_database_url
# DATABASES = {
#     'default': dj_database_url.config(conn_max_age=600, ssl_require=True)
# }

# Password validation
# https://docs.djangoproject.com/en/2.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/


STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')


STATIC_URL = '/static/'


REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework_jwt.authentication.JSONWebTokenAuthentication', # docs: http://jpadilla.github.io/django-rest-framework-jwt/#overview
    )
}

# Below code for letting the application allows login/sign up with email and password



#EMAIL_BACKEND = 'django.core.email.backends.console.EmailBackend'
EMAIL_USE_TLS = True 
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = env('EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = env('EMAIL_HOST_PASSWORD')


AUTH_USER_MODEL = 'users.CustomUser' 


AUTHENTICATION_BACKENDS = (
    # in Django admin, user still can login with username!
    "django.contrib.auth.backends.ModelBackend",
    # integrate authentication methods provided by allauth library
    "allauth.account.auth_backends.AuthenticationBackend",
)

SITE_ID = 1

REST_AUTH_REGISTER_SERIALIZERS = {
    'REGISTER_SERIALIZER': 'users.serializers.CustomRegisterSerializer',
}

ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_USERNAME_REQUIRED = False
ACCOUNT_SIGNUP_PASSWORD_PASSWORD_ENTER_TWICE = True
ACCOUNT_SESSION_REMEMBER = True
ACCOUNT_AUTHENTICATION_METHOD = 'email'
ACCOUNT_UNIQUE_EMAIL = True

# MEDIA_ROOT = '/home/phatvo/THESIS/NutrientRecommendationSystemForGDM/media/'
parent_dir = '/home/phatvo/THESIS/NutrientRecommendationSystemForGDM/'

MEDIA_ROOT = os.path.join(parent_dir, 'media/')


MEDIA_URL = '/media/'


# By default django-rest-auth uses Django’s Token-based authentication
# Use Json Web Token (JWT) for authentication instead
REST_USE_JWT = True
import datetime
JWT_AUTH = { 
    'JWT_AUTH_HEADER_PREFIX': 'Bearer', # Prefix in header should be Bearer
    'JWT_EXPIRATION_DELTA': datetime.timedelta(seconds=86400), # after 86400 sec or24 hours, the JWT should expires
    # 'USER_ID_FIELD': 'pk', # The database field from the user model that will be included in generated tokens to identify users
    # 'USER_ID_CLAIM': 'user_id',
}