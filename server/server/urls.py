## To make the email verification  
## The solutions discussed in the issue was used: https://github.com/Tivix/django-rest-auth/issues/292 
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include, re_path
from rest_framework import routers

from users.views import django_rest_auth_null, VerifyEmailView, ConfirmEmailView, UserDetailsView
# remove UserViewSet 
# Swagger/OpenAPI 2.0 Schema: https://github.com/axnsan12/drf-yasg
# To automatically generate APIs
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

from django.conf import settings
from django.conf.urls.static import static

from django.views.decorators.csrf import csrf_exempt
from food.views import DataSourceViewSet, FoodViewSet
from users.urls import urlpatterns as user_url
from nutrient.views import calculate_needed_amount_intake

schema_view = get_schema_view(
   openapi.Info(
      title="Snippets API",
      default_version='v1',
      description="Test description",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="contact@snippets.local"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

from rest_framework_jwt.views import verify_jwt_token


router = routers.DefaultRouter()
router.register(r'api/data-source', DataSourceViewSet, basename='datasource')
router.register(r'api/food', FoodViewSet, basename='food')
# router.register(r'users', UserViewSet)

urlpatterns = router.urls
from rest_auth import urls

urlpatterns = [
    path('admin/', admin.site.urls),    

    path('api-token-verify/', verify_jwt_token), # return a message "Signature has expired" if token is expired. Return status code 200 and the token if it is valid

    path('account/registration/verify-email/', VerifyEmailView.as_view(), name='rest_verify_email'),
    re_path(r'account/registration/account-confirm-email/(?P<key>[-:\w]+)/', ConfirmEmailView.as_view(), name='account_confirm_email'),
    path('account/registration/account-email-verification-sent/', django_rest_auth_null, name='account_email_verification_sent'),    
    re_path(r'^account/password-reset/confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', django_rest_auth_null, name='password_reset_confirm'),
    ## Default URLConf of rest_auth. Above code is to override default behavior.
    ## Intructions from https://github.com/Tivix/django-rest-auth/issues/292
    path('account/user/', UserDetailsView.as_view(), name='rest_user_details'),
    path('account/', include('rest_auth.urls')),
    path('account/registration/', include('rest_auth.registration.urls')),

    path('', include(router.urls)),

    path('calculator/', calculate_needed_amount_intake, name="calculator"),
    ## Add URL path, as specified in drf-yasg docs: https://github.com/axnsan12/drf-yasg
    re_path(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    # path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


urlpatterns = urlpatterns + user_url
