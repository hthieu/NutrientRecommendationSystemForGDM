"""
WSGI config for server project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/howto/deployment/wsgi/
"""

import os
from django.core.wsgi import get_wsgi_application

currentStage = os.environ.get('STAGE')


if currentStage == 'DEV':
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'server.settings.dev_settings')    
else: ## Staging
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'server.settings.staging_settings')

application = get_wsgi_application()
