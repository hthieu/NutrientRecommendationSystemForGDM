from django.contrib.auth.models import User, Group
from rest_framework import serializers

from allauth.account import app_settings as allauth_settings
from allauth.account.adapter import get_adapter
from allauth.account.utils import setup_user_email

from rest_auth.registration.serializers import RegisterSerializer

from .models import Doctor, Patient, CustomUser


# class UserSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = CustomUser
#         fields = ('username', 'email',  'is_doctor', "is_patient")

class UserDetailsSerializer(serializers.ModelSerializer):
    """
    User model w/o password
    """
    class Meta:
        model = CustomUser
        fields = ('pk', 'username', 'email', 'first_name', 'last_name', 'is_doctor', "is_patient")
        read_only_fields = ('email', )


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')

class DoctorSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Doctor
        fields = ('url',)
        
class PatientSerializer(serializers.HyperlinkedModelSerializer):
    activity = serializers.SerializerMethodField()
    stress_factor = serializers.SerializerMethodField()
        

    class Meta:
        model = Patient
        fields = ('url', 'birth_date', 'height', 'weight', 'activity', 'stress_factor')
    
    
    def get_activity(self, obj):        
        return obj.activity.__str__()

    def get_stress_factor(self, obj): 
        print(obj.stress_factor.__str__())
        return obj.stress_factor.__str__()        

## override the register serializer of django-rest-auth
## the default version can be looked at 
## https://github.com/Tivix/django-rest-auth/blob/95fafe5e0f6716296a1c664c2b870876a6b4e0cc/rest_auth/registration/serializers.py
class CustomRegisterSerializer(RegisterSerializer):
    role = serializers.CharField(write_only=True)

    def validate_role(self, role):
        if role == "doctor" or role == "patient":
            return role
        else:
            raise serializers.ValidationError("role should be doctor or patient")

    def get_cleaned_data(self):
        
        return {
            'username': self.validated_data.get('username', ''),
            'password1': self.validated_data.get('password1', ''),
            'email': self.validated_data.get('email', ''),
            'role': self.validated_data.get('role', '')
        }     
    def save(self, request):
        adapter = get_adapter()
        user = adapter.new_user(request)
        
        self.cleaned_data = self.get_cleaned_data()
        adapter.save_user(request, user, self)
        role = self.cleaned_data['role']
        if role == "doctor":
            user.is_doctor = True
            user.is_patient = False
        else:
            user.is_patient = True
            user.is_doctor  = False

        user.save()
        if (user.is_doctor):
            doctor = Doctor.objects.create(user=user)
        else: # should be patient
            patient = Patient.objects.create(user=user)
        self.custom_signup(request, user)
        setup_user_email(request, user, [])
        return user

