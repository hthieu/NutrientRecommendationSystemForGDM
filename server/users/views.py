from allauth.account.models import EmailConfirmation, EmailConfirmationHMAC
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _
# from users.serializers import UserSerializer
from rest_auth.registration.serializers import VerifyEmailSerializer
from rest_framework import status
from rest_framework.decorators import api_view, APIView
from rest_framework.permissions import IsAdminUser, AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet



from django.http import HttpResponseRedirect


from .serializers import PatientSerializer, DoctorSerializer,  UserDetailsSerializer
from .models import Patient, Doctor


'''
##  As noted by djstein in https://github.com/Tivix/django-rest-auth/issues/292,
##  this View returns response when user confirmed successfully the email
##  The response is suitable for any API use case where we only needs API
'''
@api_view()
def django_rest_auth_null(request):
    return Response(status=status.HTTP_400_BAD_REQUEST)

# class UserViewSet(ModelViewSet):
#     """
#     A simple ViewSet for viewing and editing accounts.
#     """
#     queryset = get_user_model().objects.all()
#     serializer_class = UserSerializer
#     permission_classes = [IsAdminUser]

class VerifyEmailView(APIView):
    permission_classes = (AllowAny,)
    allowed_methods = ('POST', 'OPTIONS', 'HEAD')

    def get_serializer(self, *args, **kwargs):
        return VerifyEmailSerializer(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.kwargs['key'] = serializer.validated_data['key']
        try:
            confirmation = self.get_object()
            confirmation.confirm(self.request)
            return Response({'detail': _('Successfully confirmed email.')}, status=status.HTTP_200_OK)
        except EmailConfirmation.DoesNotExist:
            return Response({'detail': _('Error. Incorrect key.')}, status=status.HTTP_404_NOT_FOUND)

    def get_object(self, queryset=None):
        key = self.kwargs['key']
        emailconfirmation = EmailConfirmationHMAC.from_key(key)
        if not emailconfirmation:
            if queryset is None:
                queryset = self.get_queryset()
            try:
                emailconfirmation = queryset.get(key=key.lower())
            except EmailConfirmation.DoesNotExist:
                raise EmailConfirmation.DoesNotExist
        return emailconfirmation

    def get_queryset(self):
        qs = EmailConfirmation.objects.all_valid()
        qs = qs.select_related("email_address__user")
        return qs
'''
## The snippet code taken from iMerica: https://gist.github.com/iMerica/a6a7efd80d49d6de82c7928140676957
## Note by Phat.Vo: When user enter the link sent to his/her email to confirm his emai, this View activates the user registered with that email
'''
class ConfirmEmailView(APIView):
    permission_classes = [AllowAny]

    def get(self, *args, **kwargs):
        self.object = confirmation = self.get_object()
        confirmation.confirm(self.request)
        # Let Client/Front end side handle the failure scenario
        # Todo!
        return HttpResponseRedirect('/login/success')

    def get_object(self, queryset=None):
        key = self.kwargs['key']
        email_confirmation = EmailConfirmationHMAC.from_key(key)
        if not email_confirmation:
            if queryset is None:
                queryset = self.get_query()
            try:
                email_confirmation = queryset.get(key=key.lower())
            except EmailConfirmation.DoesNotExist:
                # Let Client/Front end side handle the failure scenario
                # Todo!
                return HttpResponseRedirect('/login/failure/')
        return email_confirmation
    
    def get_queryset(self):
        qs = EmailConfirmation.objects.all_valid()
        qs = qs.select_related("email_addess__user")
        return qs

from rest_framework import viewsets

class PatientView(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    '''
    Get patient info, or update information
    '''
    queryset = Patient.objects.all()
    serializer_class = PatientSerializer

class DoctorView(viewsets.ModelViewSet):
    '''
    Get patient info, or update information
    '''
    queryset = Doctor.objects.all()
    serializer_class = DoctorSerializer


from rest_framework.generics import GenericAPIView, RetrieveUpdateAPIView
class UserDetailsView(RetrieveUpdateAPIView):
    """
    The custom serializer has fields is_doctor, is_patient, which are neccessary for client processing
    """
    serializer_class = UserDetailsSerializer
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        return self.request.user

    def get_queryset(self):
        """
        Adding this method since it is sometimes called when using
        django-rest-swagger
        https://github.com/Tivix/django-rest-auth/issues/275
        """
        return get_user_model().objects.none()    