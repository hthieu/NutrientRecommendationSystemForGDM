from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import CustomUser, Doctor, Patient, Counseling



class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form     = CustomUserChangeForm
    model    = CustomUser    
    list_display = ['email', 'username', 'first_name', 'last_name', 'is_doctor', 'is_patient']

class CounselingInline(admin.TabularInline):
    model = Counseling
    extra = 1
    

class CounselingAdmin(admin.ModelAdmin):
    list_display = ('getPatientName', 'getDoctorName', 'date_joined')


class DoctorAdmin(admin.ModelAdmin):
    inlines = (CounselingInline, )
    # fields = ['first_name', 'last_name', 'username', 'email', 'is_staff', 'is_active', 'date_joined', 'avatar']


class PatientAdmin(admin.ModelAdmin):
    inlines = (CounselingInline, )
    # fields = [
    #     'first_name', 'last_name', 'password',
    #     'username', 'email', 'birth_date', 
    #     'height', 'weight', 'activity', 'stress_factor', 'avatar',
    #     'is_active','date_joined']



admin.site.register(Doctor, DoctorAdmin)
admin.site.register(Patient, PatientAdmin)
# admin.site.register(Doctor)
# admin.site.register(Patient)
admin.site.register(CustomUser, CustomUserAdmin)
admin.site.register(Counseling, CounselingAdmin)

from allauth import socialaccount