from django.db import models
# from django.template.defaultfilters import slugify
from django.contrib.auth.models import AbstractUser


# from django.core.files.storage import FileSystemStorage

from factors.models import Activity, StressFactor


def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'user_{0}/{1}'.format(instance.id, filename)

# In this app, we aim to have Login via email, not username. This is a common pattern in mordern mobile web apps
# Reference: https://wsvincent.com/django-login-with-email-not-username/

### Todo:
## Reset password button for user via email : send_email button

class CustomUser(AbstractUser):
    # add additional fields in here
    is_doctor = models.BooleanField(default=False)
    is_patient = models.BooleanField(default=False)
    avatar = models.ImageField(upload_to=user_directory_path, blank=True, null=True)
    def __str__(self):
        return self.email

    class Meta:
        verbose_name = 'All User'
        verbose_name_plural = 'All Users'



# Create your models here.
class Patient(models.Model):
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE, primary_key=True)

    birth_date = models.DateField(blank=True, verbose_name="Birthdate (yyyy-mm-dd)", null=True)
    height = models.FloatField(verbose_name='Height (cm)', blank=True, null=True)
    weight = models.FloatField(verbose_name='Weight (kg)', blank=True, null=True)
    activity = models.ForeignKey(Activity, to_field='score', on_delete=models.DO_NOTHING,  blank=True, null=True)
    stress_factor = models.ForeignKey(StressFactor, to_field='score', on_delete=models.DO_NOTHING,  blank=True, null=True)
    #avatar = models.ImageField(upload_to=user_directory_path, blank=True, null=True) # note: this causes exception while saving object, probably move to User model: 'Patient' object has no attribute 'id'
    def __str__(self):
        return self.user.get_full_name() 

    
    class Meta:
        verbose_name = 'Patient'
        verbose_name_plural = 'Patients'
    

class Doctor(models.Model):   
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE, primary_key=True)

    patients = models.ManyToManyField(Patient, through='Counseling')    
    # avatar = models.ImageField(upload_to=user_directory_path)
    
    def __str__(self):
        return self.user.get_full_name()
    class Meta:
        verbose_name = 'Doctor'
        verbose_name_plural = 'Doctors'


class Counseling(models.Model):
    patient = models.ForeignKey(Patient, on_delete=models.PROTECT)
    doctor  = models.ForeignKey(Doctor, on_delete=models.PROTECT)
    date_joined = models.DateTimeField(auto_now_add=True, null=True)
    
    def getPatientName(self):
        return self.patient.__str__()

    def getDoctorName(self):
        return self.doctor.__str__()

    getPatientName.short_description = 'Patient'
    getDoctorName.short_description  = 'Treated by Doctor'
    date_joined.short_description    = " Date Joined"





        


