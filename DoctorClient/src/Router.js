// import React from 'react';
import { Scene, Router } from 'react-native-router-flux';

// import Icon from 'react-native-vector-icons/FontAwesome';

import LoginForm from './components/LoginForm';
import SignUpForm from './components/SignUpForm';

import FoodList from './components/FoodList';
import LunchFoodList from './components/LunchFoodList';
import DinnerFoodList from './components/DinnerFoodList';

import Profile from './components/Profile';

import React from 'react';

import { Text } from 'react-native';

const RouterComponent = () => {
    

    return(
        <Router>
            <Scene key="root" hideNavBar={true} initial>
                <Scene key="auth">
                    <Scene key="login" component={LoginForm} hideNavBar={true}></Scene>
                    <Scene key="signup" component={SignUpForm} hideNavBar={true}></Scene>    
                </Scene>
                
                <Scene tabs key='main' labelStyle={{ fontSize: 16, marginBottom: 15 }}>                    
                         <Scene tabBarLabel="Food Menu" initial hideNavBar={true}>
                            <Scene component={FoodList} key="foodlist" title="Food Menu" initial titleStyle={{alignSelf: 'center', justifyContent: 'center'}}  ></Scene>
                            <Scene component={LunchFoodList} key="lunchfoodlist" title="Lunch Food Menu"  titleStyle={{alignSelf: 'center', justifyContent: 'center'}}  ></Scene>
                            <Scene component={DinnerFoodList} key="dinnerfoodlist" title="Dinner Food Menu"  titleStyle={{alignSelf: 'center', justifyContent: 'center'}}  ></Scene>
                         </Scene>
                         <Scene tabBarLabel="Planned Menu" hideNavBar={true}>
                            <Scene component={FoodList} title="Food Menu" ></Scene>
                         </Scene>
                         <Scene tabBarLabel="Profile" hideNavBar={true} key='profile'>
                            <Scene component={Profile}  ></Scene>
                         </Scene>                         
                </Scene>
            </Scene>

        </Router>
    );
};

export default RouterComponent;


