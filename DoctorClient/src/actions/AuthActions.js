import { Actions } from 'react-native-router-flux';
import axios from '../components/axios';
import deviceStorage from '../services/deviceStorage'; 
import { 
  EMAIL_CHANGED,
  PASSWORD_CHANGED,
  PASSWORD2_CHANGED,
  ROLE_CHANGED,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  LOGIN_USER,
  LOGOUT_USER
} from './types';



export const emailChanged = (text) => {
  return {
    type: EMAIL_CHANGED,
    payload: text
  };
};

export const passwordChanged = (password) => {
  return {
    type: PASSWORD_CHANGED,
    payload: password
  };
};

export const password2Changed = (password2) => {
  return {
    type: PASSWORD2_CHANGED,
    payload: password2
  };
};


export const roleChanged = (selectedIndex) => {
  return {
    type: ROLE_CHANGED,
    payload: selectedIndex
  };
};



export const loginUser = ({ email, password, selectedIndex, pk}) => {
  if (selectedIndex == "0") {
    role = "doctor";
  } else {
    role = "patient"
  }
    
      
  const payload = {
    "email": email,
    "password":password,
    "role": role,
    "user_id": pk, 
    
  }


  return (dispatch) => {
    
    dispatch({ type: LOGIN_USER });     
    axios.post('account/login/',     
      payload,
    )
      .then(response => {
          axios.defaults.headers.common.Authorization = `Bearer ${response.data.token}`;
          deviceStorage.saveItem('token', response.data.token);
          deviceStorage.saveItem('user_id', String(response.data.user.pk));
          deviceStorage.saveItem('selected_id', String(selectedIndex));
          loginUserSuccess(dispatch, response.data.user, response.data.token);
      })
      .catch(function (error) {
        if (error.response) {
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
          
        } else if (error.request) {
          console.log(error.request);
        } else {
          console.log('Error', error.message);
        }
        loginUserFail(dispatch);
        console.log(error.config);        
      });
  }

};

export const loginUserFail = (dispatch) => {
  dispatch({ type: LOGIN_USER_FAIL })
};

export const loginUserSuccess = (dispatch, user, token) => { 
  dispatch( {
    type: LOGIN_USER_SUCCESS,
    user_id: user.pk,
    token: token
  });
  Actions.main();  
};

// same as loginUserSuccess, but return value, not to dispatch an action
export const markUserActive = ({user_id, token, selected_id}) => { 
  return( {
    type: LOGIN_USER_SUCCESS,
    payload: user_id,
    token: token,
    selected_id: selected_id,
  });  
};


export const logout = () => {
  deviceStorage.deleteToken();
  axios.defaults.headers.common.Authorization = '';
  Actions.auth();
  return ({
    type: LOGOUT_USER,    
  }
  );
};

export const signupUser = ({ email, password, password2, selectedIndex }) => {
  if (selectedIndex == "0") {
    role = "doctor";
  } else {
    role = "patient"
  }
    
      
  const payload = {
    "email": email,
    "password1":password,
    "password2":password2,
    "role": role,        
  }
  console.log('sign up:', payload);

  return (dispatch) => {
    
    dispatch({ type: LOGIN_USER });     
    axios.post('account/registration/',     
      payload,
    )
      .then(response => {
          axios.defaults.headers.common.Authorization = `Bearer ${response.data.token}`;
          deviceStorage.saveItem('token', response.data.token);
          deviceStorage.saveItem('user_id', String(response.data.user.pk));
          deviceStorage.saveItem('selected_id', String(selectedIndex));
          loginUserSuccess(dispatch, response.data.user, response.data.token);
      })
      .catch(function (error) {
        if (error.response) {
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
          
        } else if (error.request) {
          console.log(error.request);
        } else {
          console.log('Error', error.message);
        }
        loginUserFail(dispatch);
        console.log(error.config);        
      });
  }

};


// export const checkTokenIsValid = (token) => {
//   axios.post('api-token-verify/', {"token": token})
//   .then(response => {
//     console.log(response);
//     if (response.status == 200) {
//       // token is valid
//     } else {
//       // token is expired!
//     }
//   }).catch(function (error) {
//     console.log(error);
//   });
// }