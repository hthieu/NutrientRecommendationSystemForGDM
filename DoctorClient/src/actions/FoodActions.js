import axios from '../components/axios';



import { FOOD_FETCH_SUCCESS, FOOD_SELECT, AMOUNT_CALCULATE } from './types';


export const foodFetch = (token) => {
    const config = {
      headers: {
        Authorization: "Bearer " + token,
      }
    };

    return (dispatch) => {
        axios
          .get('api/food/', config)
          .then(response => {
            dispatch({ type: FOOD_FETCH_SUCCESS, payload: response.data });
          })
          .catch(error => {
            console.log(error);
          });
            
          
    };
};
  

export const foodSelect = (toggledData) => {
  return ({
    type: FOOD_SELECT,
    selected_food: toggledData
  });
};

export const amountCalculate = (dataToSend) => {
  // so far no data return, reducer does nothing

   const config = {
        headers: {
          Authorization: "Bearer " + token,
        }
    };

    return (dispatch) => {
        axios
          .post('calculator/', dataToSend, config)
          .then(response => {
            console.log("response");
            console.log(response);

            dispatch({ type: AMOUNT_CALCULATE, payload });
          })
          .catch(error => {
            console.log(error);
          });
            
          
    };
}