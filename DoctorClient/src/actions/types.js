export const EMAIL_CHANGED    = 'email_changed';
export const PASSWORD_CHANGED = 'password_changed';
export const PASSWORD2_CHANGED = 'password2_changed';
export const ROLE_CHANGED = 'role_changed';

export const LOGIN_USER_SUCCESS = 'login_user_success';
export const LOGIN_USER_FAIL  = 'login_user_fail';
export const LOGIN_USER = 'login_user';

export const LOGOUT='';

export const FOOD_FETCH_SUCCESS = 'food_fetch_success'; 
export const FOOD_SELECT = 'food_select'; 

export const AMOUNT_CALCULATE = 'amount_calculate';

export const ACCOUNT_GET_INFO = 'account_get_info';
export const ACCOUNT_UPDATE = 'account_update';
export const ACCOUNT_GET_PATIENT_DETAIL='account_get_patient_detail';
export const ACCOUNT_UPDATE_ACCOUNT='account_update_account';
export const UPDATE_INPUT = 'update_input';

export const LOGOUT_USER = 'logout_user';