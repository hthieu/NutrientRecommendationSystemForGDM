import { 
    ACCOUNT_GET_INFO,
    ACCOUNT_GET_PATIENT_DETAIL,
    ACCOUNT_UPDATE_ACCOUNT,
    UPDATE_INPUT
  } from './types';

import axios from '../components/axios';  

export const getUserInfo = (token) => { 
    const config = {
        headers: {
          Authorization: "Bearer " + token,
        }
    };

    return (dispatch) => {
        axios
          .get('account/user/', config)
          .then(response => {
            dispatch({ type: ACCOUNT_GET_INFO, payload: response.data });
          })
          .catch(error => {
            console.log(error);
          });
            
          
    };
};

export const getPatientDetail = (token, user_id) => { 
    const config = {
        headers: {
          Authorization: "Bearer " + token,
        }
    };

    return (dispatch) => {
        axios
          .get('patient/'+String(user_id), config)
          .then(response => {
            dispatch({ type: ACCOUNT_GET_PATIENT_DETAIL, payload: response.data });
          })
          .catch(error => {
            console.log(error);
          });
            
          
    };
};


export const updateProfile = (token, payload) => { 
    const config = {
        headers: {
          Authorization: "Bearer " + token,
        }
    };


    return (dispatch) => {
        axios
          .put('account/user/', payload, config)
          .then(response => {
            dispatch({ type: ACCOUNT_UPDATE_ACCOUNT, payload });
          })
          .catch(error => {
            console.log(error);
          });
            
          
    };
};

export const updateInput = (text, state_name) => {
  return({
    type: UPDATE_INPUT,
    text,
    state_name
  });
}