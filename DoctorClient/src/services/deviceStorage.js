// Guide from https://medium.com/@njwest/building-a-react-native-jwt-client-api-requests-and-asyncstorage-d1a20ab60cf4
import AsyncStorage from '@react-native-community/async-storage';

const deviceStorage = {
    async loadToken() {
        try {
            const token = await AsyncStorage.getItem('token');
            const user_id = await AsyncStorage.getItem('user_id');
            const selected_id = await AsyncStorage.getItem('selected_id');           
            return {user_id, token, selected_id};         
        } catch (error) {
            console.log('AsyncStorage Error: ' + error.message);
        }
    },

    async saveItem(key, value) {
        try {
          await AsyncStorage.setItem(key, value);
        } catch (error) {
          console.log('AsyncStorage Error: ' + error.message);
        }
      },
      
    async deleteToken() {
        try {
            await AsyncStorage.removeItem('token');
        } catch (error) {
            console.log('AsyncStorage Error: ' + error.message);
        }
    },    
};


export default deviceStorage;