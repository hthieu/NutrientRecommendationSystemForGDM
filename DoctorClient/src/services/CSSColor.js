const BUTTON_BG_COLOR = "#3d9fcc";
const TEXT_COLOR = "#ff6579";
// Use for Theme Provider
const Theme = {    
    Button: {
        titleStyle: {
            color: "white",     
            // width: "100%",       
        },
        buttonStyle: {
            width: "100%",
            backgroundColor:BUTTON_BG_COLOR,
        },
    },
    Text: {
        style: {
            textAlign: 'center',
            color: TEXT_COLOR,
            fontFamily: "Roboto",
        }
    }
};  

const EMERALD = "#48a9a6";
const GRAY = "#e4dfda";
const YELLOW = "#d4b483";
const RED = "#c1666b";
const CORAL = "#FF6779";
const BG = "#f2f2f2";

export { BUTTON_BG_COLOR, TEXT_COLOR, Theme, EMERALD, GRAY, YELLOW, RED, CORAL, BG };