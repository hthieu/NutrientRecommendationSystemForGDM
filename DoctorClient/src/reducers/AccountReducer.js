import {
  ACCOUNT_GET_INFO,
  ACCOUNT_GET_PATIENT_DETAIL,
  ACCOUNT_UPDATE_ACCOUNT,
  UPDATE_INPUT,
} from '../actions/types';




const INITIAL_STATE = {
    'username': '', // update request requires username
    'first_name': '',
    'last_name': '',
    'email': '',
    'password': '',
    'user_id': '',
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ACCOUNT_GET_INFO:
      const { email, first_name, last_name, pk,  is_doctor, is_patient, username } = action.payload;
      role =  is_patient?'patient':'doctor';
      return { ...state, email, first_name, last_name, user_id: pk, role, username  } ;

    case ACCOUNT_GET_PATIENT_DETAIL:
      const { height, weight, activity, stress_factor } = action.payload;            
      return { ...state, height, weight, activity, stress_factor}

    case ACCOUNT_UPDATE_ACCOUNT:
      return { ...state, ...payload }

    case UPDATE_INPUT:
      state_name = action.state_name;
      return { ...state, [state_name]: action.text}
      
      default:
      return state;
  }
};