import {
    FOOD_FETCH_SUCCESS,
    FOOD_SELECT
} from '../actions/types';
// import console = require('console');


const INITIAL_STATE = {
  foodList: {
    morning: {
      main: { },
      dessert: { }
    },
    lunch: {
      main: { },
      dessert: { }
    },
    dinner: {
      main: { },
      dessert: { }
    },  
  },  
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FOOD_FETCH_SUCCESS:
      // console.log(action.payload);
      raw_data = Array.from(action.payload);
      
      morning = [];
      lunch = [];
      dinner = [];
      dessert = [];
      
      food_data = raw_data.map(el => {
        tags = el.tags        
        el.selected = false; // newly, wonder if work          
        if (tags.search("sáng") != -1) {          
          morning.push(el);          
        }
        if (tags.search("trưa") != -1) {
          lunch.push(el);
        }
        if (tags.search("tối") != -1) {
          dinner.push(el);
        }
        if (tags.search("dessert") != -1) {
          dessert.push(el);
        }
      });
 
      return { ...state, 
        foodList: {
          "morning": {
            main: morning,
            dessert: Array.from(dessert)
          },
          "lunch": {
            main: lunch,
            dessert: Array.from(dessert)
          },
          "dinner": {
            main: dinner,
            dessert: Array.from(dessert)
          },
        } 
      };
    case FOOD_SELECT:
      // receive the key of object and boolean value of checked state!

      lastState = { ...state };
      // console.log(lastState == state?true: false);
      // console.log(lastState === state?true: false);
      
      morning = lastState.foodList.morning;

      switch (action.selected_food.meal) {
        case "morning":
          m = lastState.foodList.morning;
          break;
        case "lunch":
            m = lastState.foodList.lunch;
            break;
        case "dinner":
            m = lastState.foodList.dinner;
            break;
        default:
            m = lastState.foodList.morning;
      }
      
      if (action.selected_food.type == "main") {
          in_store_data = m.main;
      } else { // dessert
          in_store_data = m.dessert;
      }

      Object.keys(in_store_data).forEach(function (key) {
        // console.log(in_store_data[key]);
        if (in_store_data[key].id === action.selected_food.changedData.id) {
          in_store_data[key] = action.selected_food.changedData; // change the specific entry                 
        }        
        
      });

      
      if (action.selected_food.type == "main") {
        switch (action.selected_food.meal) {
          case "morning":
            lastState.foodList.morning.main = in_store_data;
            break;
          case "lunch":
            lastState.foodList.lunch.main = in_store_data;
            break;
          case "dinner":
            lastState.foodList.dinner.main = in_store_data;
            break;
          default:
            lastState.foodList.morning.main = in_store_data;
        }
      } else {
        switch (action.selected_food.meal) {
          case "morning":
              lastState.foodList.morning.dessert = in_store_data;
            break;
          case "lunch":
              lastState.foodList.lunch.dessert = in_store_data;
              break;
          case "dinner":
              lastState.foodList.dinner.dessert = in_store_data;
              break;
          default:
              lastState.foodList.morning.dessert = in_store_data;
        }        
      }
            
      
      return (Object.assign({},lastState));

    default:
      return state;
  }
};