import { 
    EMAIL_CHANGED, 
    PASSWORD_CHANGED,
    PASSWORD2_CHANGED,
    ROLE_CHANGED, 
    LOGIN_USER_SUCCESS, 
    LOGIN_USER_FAIL,
    LOGIN_USER,
    LOGOUT_USER
  } from '../actions/types';

  
  
  const INITIAL_STATE = { 
    user_id: null,
    email: '',
    password: '',
    password2: '',
    selectedIndex: 0, // chose doctor (0) or patient (1)
    // user: null,
    error:'' ,
    loading: false,
    token: '',
  };
  
  export default ( state = INITIAL_STATE, action ) => {
    switch (action.type) {
      case EMAIL_CHANGED:      
        return { ...state, email: action.payload };
      case PASSWORD_CHANGED:
        return { ...state, password: action.payload};
      case PASSWORD2_CHANGED:
        return { ...state, password2: action.payload};
      case ROLE_CHANGED:
        return { ...state, selectedIndex: action.payload};
      case LOGIN_USER:
        return { ...state, loading: true, error: ''};
      case LOGIN_USER_SUCCESS:
        console.log('action in login user');
        console.log(action);
        return { ...state, ...INITIAL_STATE, user_id: action.user_id, token: action.token, selected_id: action.selected_id};
      case LOGIN_USER_FAIL:
        return { ...state, error: 'Authentication Failed.', password: '', loading: false }
      case LOGOUT_USER:
        console.log('call me');
        return { ...INITIAL_STATE } ; // empty state, delete all previouse state!
      default:
        return state;
    }
  };