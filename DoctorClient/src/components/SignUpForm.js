// External Dependencies
import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Button, ThemeProvider, Text, Input,  ButtonGroup } from 'react-native-elements';
import { connect } from 'react-redux';
// Local Files
import { emailChanged, passwordChanged, password2Changed, roleChanged, signupUser,  markUserActive } from '../actions/AuthActions';
import deviceStorage  from '../services/deviceStorage';
import { Actions } from 'react-native-router-flux';
import { Theme } from '../services/CSSColor'; 


class SignUpForm extends Component {

    onEmailChange(text) {
        this.props.emailChanged(text);
      }

    onPasswordChange(password) {
        this.props.passwordChanged(password);
    }

    onPassword2Change(password2) {
        this.props.password2Changed(password2);   
    }
    onRoleChange(selectedIndex) {
        this.props.roleChanged(selectedIndex);
    }

    signup() {
        const { email, password, password2, selectedIndex } = this.props;
        this.props.signupUser({ email, password, password2, selectedIndex });
    }

    isAuthenticationFailed() {
        const { error } = this.props;
        if (error) {
            return <Text style={{color: 'red'}}> {error}</Text>
        }

    }

    renderSignUpButton() {
        if (this.props.loading) {
          return <Button size="large" loading />
        }
    
        return (
            <Button 
                title="SIGN UP" 
                style={{width: "100%"}}
                onPress={this.signup.bind(this)}                        
            />          
        );
      }
      

    render() {
        const buttons = ['Doctor', 'Patient'];

        
        return(
            
            <View style={styles.container}>
            <ThemeProvider theme={Theme} >
                <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#ff6579ff', '#ffb180ff']} style={styles.linearGradient}>
                    <Text h2 style={{color: "white"}}>Nutrient Adviser</Text>
                    <Text style={styles.description}>learn how much food should be taken</Text>
                </LinearGradient>
                <Text h3>Create Your Account</Text>
                <View style={styles.body}>                                        
                    <Input 
                        placeholder="email" style={{width: "10%"}}
                        leftIcon={{ type: 'font-awesome', name: 'envelope', size:18, color:"#8BB7CC" }}
                        onChangeText={this.onEmailChange.bind(this)}
                        value={this.props.email}
                    ></Input>
                    <Input placeholder="password" secureTextEntry
                           leftIcon={{ type: 'font-awesome', name: 'lock', size:30, color:"#8BB7CC" }}
                           onChangeText={this.onPasswordChange.bind(this)}
                           value={this.props.password}
                    ></Input>
                    <Input placeholder="retype password" secureTextEntry
                           leftIcon={{ type: 'font-awesome', name: 'lock', size:30, color:"#8BB7CC" }}
                           onChangeText={this.onPassword2Change.bind(this)}
                           value={this.props.password2}
                    ></Input>                    
                    <Text>You are</Text>
                    <ButtonGroup
                        onPress={this.onRoleChange.bind(this)}
                        selectedIndex={this.props.selectedIndex}
                        buttons={buttons}
                        containerStyle={{height: 25}}                        
                    />
                    <View style={{marginTop:20}}>

                        { this.renderSignUpButton() }
                        { this.isAuthenticationFailed()}
                        
                        
                        <Text style={{color: "#654321"}}>Already have an account?</Text>
                        <Button title="SIGN IN"
                                onPress={()=>Actions.login()}
                        />
                    </View>                                

                </View>
                </ThemeProvider>      
            </View>
                
          
        );
    
}
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',        
        alignItems: 'center',
    },
    linearGradient: {
        width: "100%",
        flex:2,
        justifyContent: "center",
        alignItems: "center",        
    },
    description: {
        fontSize: 16,
        color: "white",
    },
    body: {
        width: "80%",
        flex:5,
        alignItems: "center",            
        
    },
});

const mapStateToProps = ({ auth }) => {
    const { email, password, password2, selectedIndex, error, loading, token } = auth; // destructuring object  
    return { email, password, password2, selectedIndex, error, loading, token }; // return an object
  };


export default connect(mapStateToProps, {
    emailChanged,
    passwordChanged,
    password2Changed,
    roleChanged,
    signupUser,        
    markUserActive
})(SignUpForm);

