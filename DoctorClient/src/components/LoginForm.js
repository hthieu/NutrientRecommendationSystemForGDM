// External Dependencies
import React, { Component } from 'react';
import { Platform, View, StyleSheet, ProgressBarAndroid, ProgressViewIOS} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Button, ThemeProvider, Text, Input,  ButtonGroup } from 'react-native-elements';
import { connect } from 'react-redux';
// Local Files
import { emailChanged, passwordChanged, roleChanged, loginUser, loginUserSuccess, markUserActive } from '../actions/AuthActions';
import deviceStorage  from '../services/deviceStorage';
import { Actions } from 'react-native-router-flux';
import { Theme } from '../services/CSSColor'; 
import { getUserInfo } from '../actions/AccountActions';

import axios from '../components/axios';

class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            logged: false,
        }
    }

    componentWillMount() {
        this.loadToken = deviceStorage.loadToken().then(({user_id, token, selected_id}) => {
            if (token != null) {
                // we got a token from local storage, but we needs to check if it is still valid             
                axios.post('api-token-verify/', {"token": token})
                .then(response => {
                    if (response.status == 200) {
                        // token is valid
                        console.log('response status,', response.status );
                        this.props.markUserActive({user_id, token, selected_id}); 
                        this.state.logged = true;               
                        Actions.main();   
                        this.props.getUserInfo(this.props.token);                    
                    } else { // token is NOT valid
                        Actions.auth();
                    }
                }).catch(error => { console.log(error); }); 
            } 
        }).catch(err => {this.state.logged = false; console.log('error in finding token')});
    }

    onEmailChange(text) {
        this.props.emailChanged(text);
      }

    onPasswordChange(password) {
        this.props.passwordChanged(password);
    }

    onRoleChange(selectedIndex) {
        this.props.roleChanged(selectedIndex);
    }

    login() {
        const { email, password, selectedIndex } = this.props;
        this.props.loginUser({ email, password, selectedIndex });
    }

    isAuthenticationFailed() {
        const { error } = this.props;
        if (error) {
            return <Text style={{color: 'red'}}> {error}</Text>
        }

    }

    renderLoginButton() {
        if (this.props.loading) {
          return <Button size="large" loading />
        }
    
        return (
            <Button 
                title="SIGN IN" 
                style={{width: "100%"}}
                onPress={this.login.bind(this)}                        
            />          
        );
      }
      

    render() {
        const buttons = ['Doctor', 'Patient'];        
        if (!this.state.logged) { 
            // If user has logged, just show a loading circle while we loading data from server
            if (Platform.OS === 'ios') {
                return (<ProgressViewIOS style={{alignContent: "center", justifyContent: 'center', flex: 1}} />);
            }
             else {
                return (
                <View style={{alignContent: "center", justifyContent: 'center', flex: 1}} >
                    <ProgressBarAndroid />
                </View>
                );
            }
        } else {
            // If user has NOT logged, display a Login view 
            return(
                <View style={styles.container}>
                <ThemeProvider theme={Theme} >
                    <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#ff6579ff', '#ffb180ff']} style={styles.linearGradient}>
                        <Text h2 style={{color: "white"}}>Nutrient Adviser</Text>
                        <Text style={styles.description}>learn how much food should be taken</Text>
                    </LinearGradient>
                    <Text h3>Sign in to continue</Text>
                    <View style={styles.body}>                                        
                        <Input 
                            placeholder="email" style={{width: "10%"}}
                            leftIcon={{ type: 'font-awesome', name: 'envelope', size:18, color:"#8BB7CC" }}
                            onChangeText={this.onEmailChange.bind(this)}
                            value={this.props.email}
                        ></Input>
                        <Input placeholder="password" secureTextEntry
                               leftIcon={{ type: 'font-awesome', name: 'lock', size:30, color:"#8BB7CC" }}
                               onChangeText={this.onPasswordChange.bind(this)}
                               value={this.props.password}
                        ></Input>
                        <Text>You are</Text>
                        <ButtonGroup
                            onPress={this.onRoleChange.bind(this)}
                            selectedIndex={this.props.selectedIndex}
                            buttons={buttons}
                            containerStyle={{height: 25}}                        
                        />
                        <View style={{marginTop:20}}>
    
                            { this.renderLoginButton() }
                            { this.isAuthenticationFailed()}
                            <Text>Forgot your password?</Text>
                            <Text style={{color: "#654321"}}>OR</Text>
                            <Button title="SIGN UP" onPress={()=>Actions.signup()} />
                        </View>                                
    
                    </View>
                    </ThemeProvider>      
                </View>                                   
            )
        }   
}
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',        
        alignItems: 'center',
    },
    linearGradient: {
        width: "100%",
        flex:2,
        justifyContent: "center",
        alignItems: "center",        
    },
    description: {
        fontSize: 16,
        color: "white",
    },
    body: {
        width: "80%",
        flex:5,
        alignItems: "center",            
        
    },
});

const mapStateToProps = ({ auth }) => {
    const { email, password, selectedIndex, error, loading, token } = auth; // destructuring object  
    return { email, password, selectedIndex, error, loading, token }; // return an object
  };


export default connect(mapStateToProps, {
    emailChanged,
    passwordChanged,
    roleChanged,
    loginUser,
    loginUserSuccess,
    markUserActive,
    getUserInfo
})(LoginForm);

