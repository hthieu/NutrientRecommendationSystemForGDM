import React, { PureComponent } from 'react';
import { View, StyleSheet, FlatList } from 'react-native';
import { connect } from 'react-redux';
import { foodFetch, amountCalculate } from '../actions/FoodActions';
import FoodItem from './common/FoodItem';
import { Button, Text    } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import {  EMERALD, CORAL, BG, GRAY } from '../services/CSSColor';
import NavBar from './common/NavBar';


// CONSTANT DECLARATIONS
const BREAKFAST = "morning";
const LUNCH = "lunch";
const DINNER = "dinner";
const LUNCH_BTN_LABEL = "Coninue planning your lunch";
const DINNER_BTN_LABEL = "Now planning your dinner";
const FINISH_BTN_LABEL = "Finish";

const MORNING_NAV_TITLE = "Morning Menu";
const LUNCH_NAV_TITLE = "Lunch Menu";
const DINNER_NAV_TITLE = "Dinner Menu";



console.disableYellowBox = true;


class FoodList extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            mainMeal: BREAKFAST, // could be  breakfast, lunch, dinner
            displayMainMeal: true,
            btn_label: LUNCH_BTN_LABEL,
            navTitle: MORNING_NAV_TITLE,
        }
        
    }
    
    _renderItem = ({item}) => (        
        <FoodItem
            data={item}
            meal={{mainMeal: this.state.mainMeal, displayMainMeal: this.state.displayMainMeal }}
        />
      );

    componentWillMount() {
        this.props.foodFetch(this.props.token);
        
    }

    onButtonPress() {
        // console.log(this.props.selectedFood);
        // Actions.lunchfoodlist();
        if (this.state.mainMeal == BREAKFAST) {
            this.setState({
                navTitle: LUNCH_NAV_TITLE,
                mainMeal: LUNCH,
                btn_label: DINNER_BTN_LABEL,
                
            });
        } else if (this.state.mainMeal == LUNCH) {
            this.setState({
                navTitle: DINNER_NAV_TITLE,
                mainMeal: DINNER,
                btn_label: FINISH_BTN_LABEL,
            });
        } else if (this.state.mainMeal == DINNER ) {
            // this.setState({
                // mainMeal: DINNER,
                // btn_label: FINISH_BTN_LABEL,
            // });
            
            dataToSend= {
                morning: {
                    main: this.getSelectedFood(this.props.morning.main),
                    dessert: this.getSelectedFood(this.props.morning.dessert),
                },
                lunch: {
                    main: this.getSelectedFood(this.props.lunch.main),
                    dessert: this.getSelectedFood(this.props.lunch.dessert),
                },
                dinner: {
                    main: this.getSelectedFood(this.props.dinner.main),
                    dessert: this.getSelectedFood(this.props.dinner.dessert),
                },                
            };
            
            this.props.amountCalculate(dataToSend);
            
        }
    }

    getSelectedFood(listOfFood) {
        selected = []
        Array.from(listOfFood).map(el => {
            if (el.selected)
                selected.push(el.id);
        });
        return selected;

    }

    onMainBtnPress() {
        this.setState({
            displayMainMeal: true,            
        });
    }

    onDesertBtnPress() {
        this.setState({
            displayMainMeal: false,            
        });
    }





    iSMainBtnPressed() {
        console.log(this.state.displayMainMeal);
        if (this.state.displayMainMeal) {
            // console.log(this.state.mainMeal);
            switch (this.state.mainMeal) {
                case BREAKFAST:
                    // console.log("choose breakfast");
                    food_data = this.props.morning.main;
                    break;
                case LUNCH:
                    // console.log("choose lunch");
                    food_data = this.props.lunch.main;
                    break;
                case DINNER:
                    // console.log("choose dinner");
                    food_data = this.props.dinner.main;
                    break;
                default:
                    // console.log("choose default");
                    food_data = this.props.morning.main;                    
            }

            if (typeof food_data !== 'undefined' && food_data.length > 0) {
                return(
                    <FlatList style={{width: "80%", flex: 4}}
                        data={food_data}  
                        renderItem={this._renderItem}
                        keyExtractor={item => item.id.toString()}
                        // extraData={food_data}
                    />
                );
            } else {
                return (
                    <View></View>
                )
            }


        } else {
            switch (this.state.mainMeal) {
                case BREAKFAST:
                    // console.log("choose breakfast");
                    dessert = this.props.morning.dessert;
                    break;
                case LUNCH:
                    // console.log("choose lunch");
                    dessert = this.props.lunch.dessert;
                    break;
                case DINNER:
                    // console.log("choose dinner");
                    dessert = this.props.dinner.dessert;
                    break;
                default:
                    // console.log("choose default");
                    dessert = this.props.morning.dessert;                    
            }

            return(
                // <Text>X</Text>
                <FlatList style={{width: "80%", flex: 4}}
                    data={dessert}  
                    renderItem={this._renderItem}
                    keyExtractor={item => item.id.toString()}
                    // extraData={dessert}
                />
            );
        }
        
    }

    
    render() {
        return(
            <View style={styles.container}>        
                <View style={{backgroundColor: "orange", width: "100%"}}>
                    <NavBar title={this.state.navTitle} ></NavBar> 
                </View>
                 {/* // body */}
                <View style={{flexDirection: "row", width: "80%", alignItems: "center", backgroundColor:"#f2f2f2", marginBottom: 0, marginTop: 5
                              }}>
                    <View style={{width: "50%", borderWidth: 1, borderColor: EMERALD}}>
                        <Button title="Main"  titleStyle={{color: EMERALD}} type="clear" onPress={this.onMainBtnPress.bind(this)} /> 
                    </View>
                    <View style={{width: "50%", borderWidth: 1, borderColor: EMERALD}}>
                        <Button title="Dessert"  titleStyle={{color: EMERALD}} type="clear" onPress={this.onDesertBtnPress.bind(this)} /> 
                    </View>
                    
                                        
                </View>

                {this.iSMainBtnPressed()}
                {/* <FlatList style={{width: "80%", flex: 4}}
                    data={this.props.morning.main}  
                    renderItem={this._renderItem}
                    keyExtractor={item => item.id.toString()}
                    extraData={this.props.morning.main}
                /> */}

            
                <View style={{width: "80%", borderWidth: 1, borderColor: EMERALD, marginTop: 0}}>
                    <Button                 
                        buttonStyle={styles.button} 
                        title={this.state.btn_label}
                        titleStyle={{color: 'white'}}
                        onPress={this.onButtonPress.bind(this)}
                    />                    
                </View>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',        
        alignItems: "center",
        backgroundColor: "#f2f2f2f0",         
    },
    button: {
        backgroundColor: EMERALD, 
        width: "100%"
    }
});

const mapStateToProps = ( { food, auth } ) => {        
    token = auth.token;
    foodList = food.foodList;
    selectedFood = food.selectedFood;
    const { morning, lunch, dinner } = food.foodList;
    // console.log("morning", morning);
    // console.log("lunch", lunch);
    // console.log("dinner", dinner);
    
    return {  selectedFood, token, morning, lunch, dinner };
}

export default connect(mapStateToProps, { foodFetch, amountCalculate } )(FoodList);
