import React, { Component } from 'react';
import { View, StyleSheet, FlatList } from 'react-native';
import { connect } from 'react-redux';
import { foodFetch } from '../actions/FoodActions';
import FoodItem from './common/FoodItem';
import { Button, Text    } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';

import NavBar from './common/NavBar';

console.disableYellowBox = true;
class DinnerFoodList extends Component {
    
    _renderItem = ({item}) => (        
        <FoodItem
          id={item.id}
          image={item.image}          
          text={item.name}
        />
      );

    componentWillMount() {
        // console.log(this.props);
        // Actions.foodlist();
        // this.props.foodFetch(this.props.token);        
    }

    onButtonPress() {
        console.log(this.props.selectedFood);
        Actions.foodlist();
    }


    render() {
        // console.log(this.props.food_data);
        return(
            <View style={styles.container}>        
                <View style={{backgroundColor: "orange", width: "100%"}}>
                    <NavBar title="Dinner Menu"></NavBar> 
                </View>       
                <FlatList style={{width: "80%", flex: 4}}
                        data={this.props.food_data}  
                        renderItem={this._renderItem}
                        keyExtractor={item => item.id.toString()}
                />
            
                <Button  
                    buttonStyle={styles.button}
                    type="outline" 
                    title="Finish"
                    onPress={this.onButtonPress.bind(this)}
                />
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',        
        alignItems: "center",
        backgroundColor: "#f2f2f2f0",         
    },
    button: {
        marginTop: 5,
        marginBottom: 5,
    }
});

const mapStateToProps = ( { food, auth } ) => {        
    // console.log(food);
    token = auth.token;
    foodList = food.foodList;
    selectedFood = food.selectedFood;
    food_data = Array.from(foodList).map(el => {
        // id = String(el.id);
        id = el.id;
        name = el.name;
        image = el.image;
        return { id, name, image } ;
    });
    return {  food_data, selectedFood, token };
}

export default connect(mapStateToProps, { foodFetch } )(DinnerFoodList);
