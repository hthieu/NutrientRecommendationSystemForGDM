// External Dependencies
import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Theme } from '../../services/CSSColor'; 
import { ThemeProvider, Text } from 'react-native-elements';

class NavBar extends Component {

    render() {
        return(
            
            <View style={styles.container}>
            <ThemeProvider theme={Theme} >
                <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#ff6579ff', '#ffb180ff']} style={styles.linearGradient}>
                    <Text h4 style={{color: "white", fontFamily: "Roboto"}}>{this.props.title}</Text>
                    {/* <Text style={styles.description}>learn how much food should be taken</Text> */}
                </LinearGradient>
            </ThemeProvider>      
            </View>      
        );   
    }
}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',        
        alignItems: 'center',
        width: "100%",
 
    },
    linearGradient: {
        width: "100%",
        // flex:2,
        justifyContent: "center",
        alignItems: "center",        
    },
    description: {
        fontSize: 16,
        color: "white",
    },
});




export default NavBar;

