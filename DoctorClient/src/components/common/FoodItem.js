import React, { PureComponent } from 'react';
import { View, StyleSheet, Image } from 'react-native';
import { connect } from 'react-redux';
import { foodSelect } from '../../actions';

import { Button, Icon, Text, CheckBox } from 'react-native-elements'

import {  EMERALD, GRAY, YELLOW, RED } from '../../services/CSSColor';


class FoodItem extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            isChecked: this.props.data.selected,
        };
      }
      componentWillReceiveProps() {
        this.setState({isChecked: this.props.data.selected});  
      }

      componentWillMount() {
        this.setState({isChecked: this.props.data.selected});
      }

    // componentWillUpdate() {
    //     this.setState({isChecked: this.props.data.selected});  
    // }

    //   shouldComponentUpdate(preProps, nextProps){
    //       console.log('x')
    //     console.log(preProps.data.selected);
    //     console.log(nextProps.isChecked);
    //     if (preProps.data.selected != nextProps.isChecked) {
    //         return true;
    //     } else {
    //         return false;
    //     }
    //   }

    //   componentDidMount() {
    //     this.setState({isChecked: this.props.data.selected});
    //   }


    toggleButton() {
        const data = this.props.data;
        const toggledData = { changedData: { ...data, selected: !data.selected }, meal: this.props.meal.mainMeal, type: this.props.meal.displayMainMeal?"main":"dessert" } 
        this.props.foodSelect(toggledData);
        console.log(this.state.isChecked);
        x = this.state.isChecked;
        this.setState({isChecked: !x});
        console.log(this.state.isChecked);
    }

    render() {
        
        return(
            <View style={styles.container}>                
                <View style={styles.card}>
                    <Image
                      style={styles.image}
                      source={{uri: this.props.data.image}}
                      >
                    </Image>
                    <View style={styles.text}>
                        <Text>
                            {this.props.data.name}
                        </Text>
                    </View>                    

                    <CheckBox
                        center
                        // title='Click Here'                        
                        size={30}
                        checkedIcon='check-circle'
                        uncheckedIcon='circle-o'
                        uncheckedColor='#15aaf7f0'
                        checked={this.state.isChecked}
                        onPress={this.toggleButton.bind(this)}
                    />                    
                </View>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {        
        // flexDirection: 'column',        
        // alignItems: "center",
        backgroundColor: "#f2f2f2",         
    },
    card: {
        flexDirection: 'row',
        height: 100,
        backgroundColor: "#f2f2f2", // cfcfcf 
        justifyContent: 'center',        
        alignItems: 'center',        
        borderWidth: 1,
        borderTopWidth: 0,
        borderColor: EMERALD,
        // width: "80%",     
    },
    image: {
        width: 100, 
        height: "100%",        
    },
    text: {
        fontSize: 16,
        color: "white",
        flex: 4,
    },
    button: {
        marginRight: 20,
        backgroundColor: 'white',
        borderColor: '#15aaf7',
        width: 35,
        height: 35,
        borderRadius: 100,
        borderWidth: 1,
    }    
});

const mapStateToProps = ( { food} ) => {        
    foodList = food.foodList;
    selectedFood = food.selectedFood;
    const { morning, lunch, dinner } = food.foodList;
    console.log(morning.main[0].selected);
    
    return {  selectedFood, token, morning, lunch, dinner };
}

export default connect(null,{ foodSelect })(FoodItem);
