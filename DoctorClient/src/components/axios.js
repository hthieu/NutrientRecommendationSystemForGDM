import * as axios from 'axios';

SERVER_URL = "https://nutrient-recommendation.herokuapp.com/"
// the IP 10.0.2.2 is the IP the Android simulator uses to communicate with the localhost 
LOCAL_URL = "http://10.0.2.2:8000/"
// use this url when want to run the the app in the physical android device, 
// given the server is run at localhost, port 8000, run this command to map the correct IP: adb reverse tcp:8000 tcp:8000
// LOCAL_URL = "http://127.0.0.1:8000/" // 

var instance = axios.create();


// instance.defaults.baseURL = 'http://10.0.2.2:8000/';
instance.defaults.baseURL = LOCAL_URL
instance.defaults.timeout = 10000 //8000;
// instance.defaults.proxy =  false;

instance.defaults.xsrfCookieName = "csrftoken";
instance.defaults.xsrfHeaderName = "X-CSRFTOKEN"

instance.defaults.withCredentials = true

// Add a request interceptor
instance.interceptors.request.use(function (config) {
    // Do something before request is sent
    console.log('intercept .. request');
    console.log(config);
    return config;
  }, function (error) {
    // Do something with request error
    return Promise.reject(error);
  });

// Add a response interceptor
instance.interceptors.response.use(function (response) {
    // Do something with response data
    console.log('intercept .. response');
    console.log(response);    
    return response;
  }, function (error) {
    // Do something with response error
    return Promise.reject(error);
  });

export { instance as default };
