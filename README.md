The project is currently hosted at two place:
- Environment: https://nutrient-recommendation.herokuapp.com/


Staging Environment corresponds to staging branch. After newly implemented features are ready, I will push the code to master branch. A message will be sent to Slack channel as notification.

The APIs endpoint documentation is located at: 
- https://nutrient-recommendation.herokuapp.com/redoc/



The demo of Genetic Algorithm is located at server/ga_tools.ipynb
It can only run locally.

#### Export Database relation: (Devepoment enviroment):
I used a library called pygraphviz to export the database diagram.
To install it in your enviroment, 
In the terminal, run the the following command to install the dependencies:
```bash
    sudo apt-get install graphviz libgraphviz-dev pkg-config
```    

To export the database diagram, the command below can be used 
```bash                    
    python manage.py graph_models -a -g -o database-diagram.png
```
